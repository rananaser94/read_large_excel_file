import pandas as pd
#print all file in the directory

def process_chunk(chunk):
    print(chunk.head(10))

df = pd.read_excel('Large.xlsx', sheet_name='Sheet1', header=0, usecols='A:E')
#read all data
#print(df)


#print first 10 rows
print(df.head(10))


#read chuncks of the file
for chunk in pd.read_excel('large_file.xlsx', sheet_name='Sheet1', header=0, usecols='A:E', chunksize=1000):
    process_chunk(chunk)
