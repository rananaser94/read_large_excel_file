
import pandas as pd
import openpyxl

#read the large excel file
wb = openpyxl.load_workbook(filename="Large.xlsx", read_only=True)
ws = wb.active
# Load the rows
rows = ws.rows
first_row = [cell.value for cell in next(rows)]
# Load the data
data = []
for row in rows:
    record = {}
    for key, cell in zip(first_row, row):
        record[key] = cell.value
    data.append(record)


# Convert to a df
df = pd.DataFrame(data)
#print some of rows with conditions on timestamp_1
print("row in timeStamp_1 between 2021-05-18 and 2021-05-20 \n")
range = (df['timestamp_1'] > '2021-05-18') & (df['timestamp_1'] <= '2021-05-20')
pd.set_option('display.width', 1000)
pd.set_option('display.max_columns', None)
pd.set_option('display.max_colwidth',20)
pd.set_option('display.max_rows', None)
print(df.loc[range])