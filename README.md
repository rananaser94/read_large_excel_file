# Read_Large_Excel_File



## Getting started

run LargeExcel.py to create large excel file with random data and headers
the file will contain 1.5 Milion rows and 7 columns


Now: we can read large excel file by **python** since MS excel program will delete any row after 1M 
By  python we can read large excel file by **openpyxl** package Or **pandas** package

Reading by **openpyxl** package: ReadLargeExcelByOpenpyxl.py
this code will print rows where the condition applys


Reading by **pandas** package: ReadLargeExcelBypandas.py
we can read all rows or chunks. 
print first NO. of rows 
process chunks
